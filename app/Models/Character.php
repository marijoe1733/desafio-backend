<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model{
    protected $table = "character";

    public function episodes()
    {
        return $this->belongsToMany(Episode::class, 'characters_episodes', 'episode_id', 'character_id');
    }
}