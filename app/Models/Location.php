<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model{
    protected $table = "location";

    public function character()
    {
        return $this->hasMany(Location::class);
    }
}