<?php   
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model{
    protected $table = "episodes";

    public function characters()
    {
        return $this->belongsToMany(Character::class, 'characters_episodes', 'character_id','episode_id');
    }
}