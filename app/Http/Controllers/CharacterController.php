<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Character;
use App\Models\Episodes;
use App\Models\Location;
use Illuminate\Http\Response;





class CharacterController extends Controller
{
    public function getcharacters()
    {
        $data = Character::all(); 


       /* $data = Location::join('character','location.id','=','character.location')
        ->select('character.id as id','location.name as nameLocation','character.name as nameCharacter','character.status as status','character.image as image')
        ->get();*/

        if($data == false){
            return response()->json(["error"=>[
                "message"=> "This  character list cannot be found"
            ]], 404);  
        }else{
            return  response()->json($data, 200);

        }  
    }

    public function updateCharacter(Request $request, $id){


      $data = Character::where('id', $id)->update(['name'=>$request->nameCharacter,'location'=>$request->locationCharacter]);
      if($data == false){
        return response()->json(["error"=>[
            "message"=> "error"
        ]], 404);  
    }else{
        return  response()->json($data, 200);

    }  


    }
      



}