<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('prueba', [App\Http\Controllers\PruebaController::class, 'index']);
Route::get('episodes', [App\Http\Controllers\EpisodeController::class, 'index']);
Route::get('characters', [App\Http\Controllers\CharacterController::class, 'getCharacters']);
Route::put('character/{id}', [App\Http\Controllers\CharacterController::class, 'updateCharacter']);

