<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CharacterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('character')->insert(['name' => 'Homer Simpson',
                                         'status' => 'Alive',
                                         'species' => 'Human', 
                                         'type' => 'Protagonist',
                                         'gender' => 'Male',
                                         'origin' => 'Panama',
                                         'location' => 'London',
                                         'image' => 'https://www.estilodf.tv/wp-content/uploads/2018/05/homero.jpg',
                                         //'episode' => 'S01001,S01002,S01003,S01002,S01002,S01002,S01002',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Bart Simpson',
                                         'status' => 'unknown',
                                         'species' => 'Human', 
                                         'type' => 'Protagonist',
                                         'gender' => 'Male',
                                         'origin' => 'Roma',
                                         'location' => 'Casa Blanca',
                                         'image' => 'https://mantenlosimpleblog.files.wordpress.com/2014/06/bart-simpson-principal.png',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Marge Simpson',
                                         'status' => 'unknown',
                                         'species' => 'Human', 
                                         'type' => 'Protagonist',
                                         'gender' => 'Female',
                                         'origin' => 'Dakar',
                                         'location' => 'Adis Abeba',
                                         'image' => 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/marge-simpson-1569921773.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Lisa Simpson',
                                         'status' => 'Alive',
                                         'species' => 'Human', 
                                         'type' => 'Love interest',
                                         'gender' => 'Female',
                                         'origin' => 'Caracas',
                                         'location' => 'Santori',
                                         'image' => 'https://i.pinimg.com/474x/63/64/b9/6364b9303c615663c91d91096750aa76.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Montgomery',
                                         'status' => 'Alive',
                                         'species' => 'Human', 
                                         'type' => 'Round character',
                                         'gender' => 'Male',
                                         'origin' => 'Caledonia',
                                         'location' => 'Delhi',
                                         'image' => 'https://assets.entrepreneur.com/content/3x2/2000/20180419143111-burns.jpeg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Sideshow Bob',
                                         'status' => 'Dead',
                                         'species' => 'Human', 
                                         'type' => 'Antagonist',
                                         'gender' => 'Male',
                                         'origin' => 'Delhi',
                                         'location' => 'Ottawa',
                                         'image' => 'https://pbs.twimg.com/profile_images/520169180276858880/ZohoIXFx_400x400.jpeg',
                                         //'episode' => 'S01001,S01002,S01003,S01002,S01002,S01002,S01002',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
                                         DB::table('character')->insert(['name' => 'Apu Nahasapeem',
                                         'status' => 'unknown',
                                         'species' => 'Human', 
                                         'type' => 'Protagonist',
                                         'gender' => 'Male',
                                         'origin' => 'Reikiavik',
                                         'location' => 'Maseru',
                                         'image' => 'https://indiehoy.com/wp-content/uploads/2018/01/Apu-Nahasapeemapetilon.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                          DB::table('character')->insert(['name' => 'Ned Flanders',
                                         'status' => 'Dead',
                                         'species' => 'Human', 
                                         'type' => 'Tertiary characters',
                                         'gender' => 'Male',
                                         'origin' => 'Madrid',
                                         'location' => 'Reikiavik',
                                         'image' => 'http://pm1.narvii.com/7148/6483cdcc7e1ba38f46f59aa19facd9dcef0a638ar1-900-900v2_00.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
                                         DB::table('character')->insert(['name' => 'Edna Krabappel',
                                         'status' => 'Dead',
                                         'species' => 'Human', 
                                         'type' => 'Stock character',
                                         'gender' => 'Female',
                                         'origin' => 'PAris',
                                         'location' => 'Reikiavik',
                                         'image' => 'https://laverdadnoticias.com/__export/1613656017054/sites/laverdad/img/2021/02/18/los_simpson_edna_krabappel.jpg_423682103.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                          DB::table('character')->insert(['name' => 'Allison Taylor',
                                         'status' => 'Dead',
                                         'species' => 'Human', 
                                         'type' => 'Protagonist',
                                         'gender' => 'Female',
                                         'origin' => 'Yakarta',
                                         'location' => 'Madrid',
                                         'image' => 'https://lagranfamiliasimpson.files.wordpress.com/2012/01/allison-taylor.jpg',
                                         //'episode' => 'S01001,S01002,S01003,S01002,S01002,S01002,S01002',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
                                         DB::table('character')->insert(['name' => 'Paty Bouvier',
                                         'status' => 'Dead',
                                         'species' => 'Human', 
                                         'type' => 'Symbolic character',
                                         'gender' => 'Female',
                                         'origin' => 'Santo Domingo',
                                         'location' => 'Pretoria',
                                         'image' => 'https://www.clipartmax.com/png/middle/6-60315_jacqueline-bouvier-%E2%80%A2-selma-bouvier-selma-bouvier.png',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                         DB::table('character')->insert(['name' => 'Seymour Skinner',
                                         'status' => 'unknown',
                                         'species' => 'Human', 
                                         'type' => 'Stock character',
                                         'gender' => 'Male',
                                         'origin' => 'La Paz',
                                         'location' => 'Merida',
                                         'image' => 'http://2.bp.blogspot.com/-PqTCfWtOiJE/T4STwSt0cWI/AAAAAAAAAvA/bXEpCUXlMxM/s1600/Seymour_Skinner.png',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
                                         DB::table('character')->insert(['name' => 'Willie',
                                         'status' => 'Alive',
                                         'species' => 'Human', 
                                         'type' => 'Round character',
                                         'gender' => 'Male',
                                         'origin' => 'Bagday',
                                         'location' => 'Bogota',
                                         'image' => 'https://i.pinimg.com/originals/0b/0d/78/0b0d78cc5e01e04ef5243a0455f2b961.png',
                                         //'episode' => 'S01001,S01002,S01003,S01002,S01002,S01002,S01002',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

                                          DB::table('character')->insert(['name' => 'Krusty el payaso',
                                         'status' => 'unknown',
                                         'species' => 'Human', 
                                         'type' => 'Deuteragonists',
                                         'gender' => 'Male',
                                         'origin' => 'Tokyo',
                                         'location' => 'Pekin',
                                         'image' => 'https://images-na.ssl-images-amazon.com/images/I/61QsYprbyqL._AC_SX425_.jpg',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
                                         DB::table('character')->insert(['name' => 'Little Helper ',
                                         'status' => 'Alive',
                                         'species' => 'Animal', 
                                         'type' => 'Foil',
                                         'gender' => 'Male',
                                         'origin' => 'Sydney',
                                         'location' => 'Guadalajara',
                                         'image' => 'https://www.clipartmax.com/png/middle/33-330749_the-simpsons-clipart-santas-little-helper-santas-little-helper-from-the-simpsons.png',
                                         'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
       
      
    }
}
