<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EpisodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('episodes')->insert(['episode' => 'S01E01','name' => 'Especial de navidad','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E02','name' => 'The Luminous Fish Effect','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E03','name' => 'The Hamburger Postulate','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E04','name' => 'The Dumpling Paradox','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E05','name' => 'The Grasshopper Experiment','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E06','name' => 'The Pork Chop Indeterminacy','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E07','name' => 'The Barbarian Sublimation','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E08','name' => 'Principal Charming','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E09','name' => 'Especial de Febrero','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E10','name' => 'Especial de pascua','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S01E11','name' => 'Especial de Marzo','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E01','name' => 'El cumpleanos de Sara','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E02','name' => 'La revolucion','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E03','name' => 'Especial de Mayo','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E04','name' => 'Especial de navidad','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E05','name' => 'El nacimiento','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
       /* DB::table('episodes')->insert(['episode' => 'S02E06','name' => 'San Valentin','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E07','name' => 'La muerto del Abuelo','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E08','name' => 'Dia de los muerto','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E09','name' => 'Primer dia de clases','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E10','name' => 'Especial Agosto','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E11','name' => 'Semana Santa','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E12','name' => 'La tasa de cafe','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('episodes')->insert(['episode' => 'S02E13','name' => 'La boda','url' => 'https://www.youtube.com/watch?v=EiEvnRuCJqA','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);*/
        
    }
}
