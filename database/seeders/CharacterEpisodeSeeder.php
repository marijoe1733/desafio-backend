<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CharacterEpisodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('characters_episodes')->insert(['character_id' => '15','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '10','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '12','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        

        DB::table('characters_episodes')->insert(['character_id' => '14','episode_id' => '2','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '7','episode_id' => '2','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '10','episode_id' => '2','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '6','episode_id' => '2','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '2','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
       
        DB::table('characters_episodes')->insert(['character_id' => '14','episode_id' => '3','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '7','episode_id' => '3','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '10','episode_id' => '3','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '6','episode_id' => '3','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '3','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
       
        DB::table('characters_episodes')->insert(['character_id' => '3','episode_id' => '4','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '4','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '4','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '4','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '4','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '5','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '5','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '5','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '3','episode_id' => '5','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '5','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);



        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '6','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '6','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '7','episode_id' => '6','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '6','episode_id' => '6','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '6','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '7','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '7','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '15','episode_id' => '7','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '8','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '8','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '3','episode_id' => '9','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '9','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '9','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '9','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '9','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '10','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '15','episode_id' => '10','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '10','episode_id' => '10','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
        DB::table('characters_episodes')->insert(['character_id' => '6','episode_id' => '11','created_at' => Carbon::now()->format('Y-m-d H:i:s')]); 
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '11','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '3','episode_id' => '11','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '1','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
       
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '12','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '12','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '12','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);


        DB::table('characters_episodes')->insert(['character_id' => '13','episode_id' => '13','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '7','episode_id' => '13','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '9','episode_id' => '13','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '14','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '2','episode_id' => '14','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '3','episode_id' => '14','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '15','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '15','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '15','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '15','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);


       
        DB::table('characters_episodes')->insert(['character_id' => '1','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '4','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '8','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '5','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '7','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '12','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('characters_episodes')->insert(['character_id' => '15','episode_id' => '16','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);



      

       


    }
}
