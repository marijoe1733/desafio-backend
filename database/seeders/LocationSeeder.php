<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location')->insert(['name' => 'Guadalajara','type' => 'Locality','dimension' => '100', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Sydney','type' => 'Relative location','dimension' => '200', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Yakarta','type' => 'Locality','dimension' => '70', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Pretoria','type' => 'Absolute location','dimension' => '100', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Reikiavik','type' => 'Locality','dimension' => '150', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Tokyo','type' => 'Relative location','dimension' => '300', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Dakar','type' => 'Absolute location','dimension' => '420', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Madrid','type' => 'Relative location','dimension' => '40', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Paris','type' => 'Locality','dimension' => '370', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('location')->insert(['name' => 'Maseru','type' => 'Absolute location','dimension' => '610', 'residents' => '','url' => 'https://www.waze.com/live-map','created_at' => Carbon::now()->format('Y-m-d H:i:s')]);




    }
}
